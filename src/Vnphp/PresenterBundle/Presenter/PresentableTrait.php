<?php


namespace Vnphp\PresenterBundle\Presenter;

trait PresentableTrait
{
    /**
     * @var Presenter
     */
    protected $presenter;

    /**
     * @param mixed $presenter
     */
    public function setPresenter(Presenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * @return Presenter|null
     */
    public function getPresenter()
    {
        return $this->presenter;
    }
}
