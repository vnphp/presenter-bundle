<?php

namespace Vnphp\PresenterBundle\Presenter;

class Presenter
{
    /**
     * @var mixed
     */
    protected $subject;

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->subject, $name], $arguments);
    }
}
