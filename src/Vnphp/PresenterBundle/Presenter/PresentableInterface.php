<?php


namespace Vnphp\PresenterBundle\Presenter;

interface PresentableInterface
{
    /**
     * @return Presenter|null
     */
    public function getPresenter();

    /**
     * @param Presenter $instance
     */
    public function setPresenter(Presenter $instance);

    /**
     * @return string service name for presenter
     */
    public function getPresenterService();
}
