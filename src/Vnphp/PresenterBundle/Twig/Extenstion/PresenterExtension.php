<?php


namespace Vnphp\PresenterBundle\Twig\Extenstion;

use Vnphp\PresenterBundle\Factory\PresenterFactoryInterface;

class PresenterExtension extends \Twig_Extension
{
    /**
     * @var PresenterFactoryInterface
     */
    protected $factory;

    /**
     * PresenterExtension constructor.
     * @param PresenterFactoryInterface $factory
     */
    public function __construct(PresenterFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('presenter', [$this->factory, 'getPresenter']),
        ];
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'vnphp_presenter';
    }
}
