<?php


namespace Vnphp\PresenterBundle\Tests\Presenter;

use Vnphp\PresenterBundle\Tests\Fixtures\Model\User;
use Vnphp\PresenterBundle\Tests\Fixtures\Presenter\UserPresenter;

class PresenterTest extends \PHPUnit_Framework_TestCase
{
    public function testCallSubjectMethod()
    {
        $user = new User();
        $user->setFirstName('git');

        $instance = new UserPresenter();
        $instance->setSubject($user);

        static::assertEquals($user->getFirstName(), $instance->getFirstName());
    }

    public function testCallMethod()
    {
        $user = new User();
        $user->setFirstName('git');
        $user->setLastName('vcs');

        $instance = new UserPresenter();
        $instance->setSubject($user);

        static::assertEquals('git vcs', $instance->getFullName());
    }
}
