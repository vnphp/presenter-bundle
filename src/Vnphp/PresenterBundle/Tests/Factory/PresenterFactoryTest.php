<?php


namespace Vnphp\PresenterBundle\Tests\Factory;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Vnphp\PresenterBundle\Factory\PresenterFactory;
use Vnphp\PresenterBundle\Tests\Fixtures\Model\User;
use Vnphp\PresenterBundle\Tests\Fixtures\Presenter\UserPresenter;

class PresenterFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PresenterFactory
     */
    protected $instance;

    /**
     * @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $container;

    protected function setUp()
    {
        $this->container = $this->getMockBuilder(ContainerInterface::class)
            ->getMock();
        $this->instance = new PresenterFactory($this->container);
    }

    public function testGetPresenter()
    {
        $this->container->expects(static::once())
            ->method('get')
            ->with('presenter.user')
            ->will(static::returnValue(new UserPresenter()));

        $model = new User();
        $presenter1 = $this->instance->getPresenter($model);
        static::assertInstanceOf(UserPresenter::class, $presenter1);

        $presenter2 = $this->instance->getPresenter($model);
        static::assertSame($presenter1, $presenter2);
    }
}
