<?php


namespace Vnphp\PresenterBundle\Tests\Fixtures\Presenter;

use Vnphp\PresenterBundle\Presenter\Presenter;

class UserPresenter extends Presenter
{
    public function getFullName()
    {
        return "{$this->getFirstName()} {$this->getLastName()}";
    }
}
