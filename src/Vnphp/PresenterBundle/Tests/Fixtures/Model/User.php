<?php


namespace Vnphp\PresenterBundle\Tests\Fixtures\Model;

use Vnphp\PresenterBundle\Presenter\PresentableInterface;
use Vnphp\PresenterBundle\Presenter\PresentableTrait;

class User implements PresentableInterface
{
    use PresentableTrait;

    private $firstName;

    private $lastName;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string service name for presenter
     */
    public function getPresenterService()
    {
        return 'presenter.user';
    }
}
