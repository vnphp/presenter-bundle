<?php


namespace Vnphp\PresenterBundle\Factory;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Vnphp\PresenterBundle\Presenter\PresentableInterface;

class PresenterFactory implements PresenterFactoryInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * PresenterFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getPresenter(PresentableInterface $entity)
    {
        $instance = $entity->getPresenter();
        if (!$instance) {
            $instance = $this->container->get($entity->getPresenterService());
            $instance->setSubject($entity);
            $entity->setPresenter($instance);
        }
        return $instance;
    }
}
