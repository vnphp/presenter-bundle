<?php

namespace Vnphp\PresenterBundle\Factory;

use Vnphp\PresenterBundle\Presenter\PresentableInterface;

interface PresenterFactoryInterface
{
    public function getPresenter(PresentableInterface $entity);
}
