# Vnphp Presenter bundle


[![build status](https://gitlab.com/vnphp/presenter-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/presenter-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/4031d541-e125-4f5c-9b76-e8f5636c449f/big.png)](https://insight.sensiolabs.com/projects/4031d541-e125-4f5c-9b76-e8f5636c449f)


## Installation 

```
composer require vnphp/presenter-bundle
```

You need to have `git` installed. 

## Configure

### 1. Add the bundle to your `AppKernel.php`:

```php
<?php 

$bundles = array(          
            new \Vnphp\PresenterBundle\VnphpPresenterBundle(),
        );

```

### 2. Update your model
```php
<?php

use Vnphp\PresenterBundle\Presenter\PresentableInterface;
use Vnphp\PresenterBundle\Presenter\PresentableTrait;

class User implements PresentableInterface
{
    use PresentableTrait;
    
    /**
     * @return string service name for presenter
     */
    public function getPresenterService()
    {
        return 'presenter.user';
    }
}
```

### 3. Create presenter class
```php
<?php

use Vnphp\PresenterBundle\Presenter\Presenter;

class UserPresenter extends Presenter
{
    public function getFullName()
    {
        return "Full Name";
    }
}
```

### 4. Add presenter service.

It is required to set `scope: prototype` to the definition.

```yaml
services:
    presenter.user:
        class: 'UserPresenter'
        scope: 'prototype'
```


## Usage

### Controller

```php
<?php

$userPresenter = $this->container->get('vnphp_presenter.factory')
    ->getPresenter($user);
$userPresenter->getFullName();
```

### Twig
```twig
{% set user_presenter = presenter(user) %}
{{ user_presenter.fullName }}
```
